use std::collections::HashMap;
use std::path::PathBuf;
use std::sync::{Arc, Mutex};
use rocket::fs::{FileServer, Options};
use rocket::launch;
use uuid::Uuid;

use game::GameManager;
use crate::routes::routes;
use crate::ws::proxy::ClientProxy;
use crate::ws::ws_client;

mod routes;
mod models;
mod ws;
mod game;

pub type ManagedState = Arc<Mutex<State>>;

pub struct State {
    pub proxies: HashMap<Uuid, ClientProxy>,
    pub game_manager: GameManager,
}

impl State {
    fn new() -> Self {
        State {
            proxies: HashMap::new(),
            game_manager: GameManager::new(),
        }
    }
}

#[launch]
fn rocket() -> _ {
    let state = ManagedState::new(Mutex::new(State::new()));

    tokio::spawn(ws_client(state.clone()));

    rocket::build()
        .mount("/", FileServer::new(PathBuf::from(option_env!("FILE_SERVER_PATH").unwrap_or("./")), Options::Index))
        .mount("/", routes())
        .manage(state)
}