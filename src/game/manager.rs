use std::collections::HashMap;
use uuid::Uuid;
use crate::ClientProxy;
use crate::game::Game;
use crate::models::Error;
use crate::ws::message::FromClientMessage;
use crate::ws::message::Response;

/// Manages all games
#[derive(Default)]
pub struct GameManager {
    games: HashMap<Uuid, Game>,
}

fn flatten<V, E>(res: Result<Result<V, E>, E>) -> Result<V, E> {
    match res {
        Ok(res) => res,
        Err(e) => Err(e),
    }
}

impl GameManager {
    pub fn new() -> GameManager {
        GameManager {
            games: HashMap::new()
        }
    }

    /// Process the message if possible, or dispatches it to the sender's game
    pub fn receive(&mut self, sender_id: Uuid, sender_proxy: ClientProxy, msg: FromClientMessage) -> Result<Response, Error> {
        match msg {
            FromClientMessage::JoinGame(game_id) => {
                flatten(self.games.iter_mut()
                    .find(|(id, _)| **id == game_id)
                    .map(|(_, g)| g.add_player(sender_id.clone(), sender_proxy).then(|| Response::Ok).ok_or(Error::OperationImpossible))
                    .ok_or(Error::NoSuchGame))
            }
            _ => {
                flatten(self.games.values_mut()
                    .find(|g| g.has_player(&sender_id))
                    .map(|g| g.receive(&sender_id, msg))
                    .ok_or(Error::PlayerNotInGame))
            }
        }
    }
}
