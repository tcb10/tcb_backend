mod manager;

pub use manager::GameManager;

use uuid::Uuid;
use crate::ClientProxy;
use crate::models::Error;
use crate::ws::message::{FromClientMessage, Response};

pub struct Game;

impl Game {
    pub fn has_player(&self, uuid: &Uuid) -> bool {
        false
    }

    pub fn add_player(&mut self, uuid: Uuid, proxy: ClientProxy) -> bool { false }

    pub fn receive(&mut self, sender: &Uuid, msg: FromClientMessage) -> Result<Response, Error> { Err(Error::Other) }
}