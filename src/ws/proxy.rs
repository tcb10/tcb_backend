use futures::SinkExt;
use futures_channel::mpsc::UnboundedSender;
use log::debug;
use crate::ws::message::ToClientMessage;

/// Proxy to safely send message to a client through a websocket
#[derive(Clone)]
pub struct ClientProxy {
    handle: UnboundedSender<ToClientMessage>,
}

impl ClientProxy {
    pub(crate) fn new(handle: UnboundedSender<ToClientMessage>) -> ClientProxy {
        ClientProxy {
            handle
        }
    }

    /// Send a message to the client
    ///
    /// Returns whether the operation was successful
    pub fn send(&mut self, msg: ToClientMessage) -> bool {
        if let Err(e) = futures::executor::block_on(self.handle.send(msg)) {
            debug!("Could not send message to client.\nCause:{:?}", e);
            false
        } else { true }
    }
}
