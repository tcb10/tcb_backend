pub mod proxy;
pub mod message;

use std::net::SocketAddr;
use futures::{future, pin_mut, StreamExt, TryStreamExt};
use futures_channel::mpsc::unbounded;
use log::error;
use tokio::net::{TcpListener, TcpStream};
use tokio_tungstenite::tungstenite::Message;
use uuid::Uuid;
use message::ToClientMessage;
use crate::ManagedState;
use crate::ws::proxy::ClientProxy;

async fn handle_connection(state: ManagedState, stream: TcpStream, addr: SocketAddr) -> Result<(), ()> {
    // Converts TcpStream to WebSocketStream
    let stream = tokio_tungstenite::accept_async(stream).await
        .map_err(|e| error!("Could not accept connection from {}. Cause: {:?}", addr, e))?;

    // Create mpsc to transmit messages to the socket
    let (tx, rx) = unbounded::<ToClientMessage>();
    let (outgoing, incoming) = stream.split();

    // Generate the player id TODO too local ?
    let id = Uuid::new_v4();

    // Create the proxy and adds to the online proxies
    let proxy = ClientProxy::new(tx);
    state.lock().unwrap().proxies.insert(id, proxy.clone());

    // Redirect mpsc to WebSocketStream
    let to_client = rx.map(|msg| {
        Ok(Message::from(serde_json::to_string(&msg).map_err(|_| tokio_tungstenite::tungstenite::Error::Utf8)?))
    }).forward(outgoing);

    // Handle message from the websocket
    let from_client = incoming.try_for_each(|msg| {
        if let Ok(msg) = msg.try_into() {
            match state.lock().unwrap().game_manager.receive(id, proxy.clone(), msg) {
                Ok(r) => {}
                Err(e) => {}
            }
        }
        future::ok(())
    });

    // Wait for either of the task to end
    pin_mut!(to_client, from_client);
    future::select(to_client, from_client).await;

    // Remove the proxy
    state.lock().unwrap().proxies.remove(&id);

    Ok(())
}

/// Handle incoming websocket connection for a given state
///
/// Adds a proxy to the state for each new connection, and removes it when disconnecting
pub async fn ws_client(state: ManagedState) {
    let listener = TcpListener::bind(option_env!("WS_ADDR").unwrap_or("127.0.0.1:8001")).await.expect("Could not init ws listener");
    while let Ok((stream, addr)) = listener.accept().await {
        tokio::spawn(handle_connection(state.clone(), stream, addr));
    }
}
