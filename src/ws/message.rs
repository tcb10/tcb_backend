use uuid::Uuid;
use tokio_tungstenite::tungstenite::Message;
use crate::models::Error;
use serde::{Serialize, Deserialize};

/// Message to send to the client
#[derive(Debug, Serialize)]
pub enum ToClientMessage {
    Response(Response),
    Error(Error),
}

/// Response to a FromClientMessage
#[derive(Debug, Serialize)]
pub enum Response{
    Ok
}

/// Intermediary step for FromClientMessage deserialization
#[derive(Deserialize)]
enum DeFromClientMessage {
    JoinGame(u128),
}

/// Request from the client
#[derive(Debug)]
pub enum FromClientMessage {
    JoinGame(Uuid)
}

impl TryFrom<Message> for FromClientMessage {
    type Error = ();

    fn try_from(value: Message) -> Result<Self, Self::Error> {
        Ok(match serde_json::from_str::<DeFromClientMessage>(value.to_text().map_err(|_| ())?).map_err(|_| ())? {
            DeFromClientMessage::JoinGame(u) => FromClientMessage::JoinGame(Uuid::from_u128(u))
        })
    }
}
