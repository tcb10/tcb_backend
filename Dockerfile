FROM rust:latest as builder

WORKDIR /app

# create a new empty project
RUN cargo init

#COPY ./.cargo .cargo
#COPY ./vendor vendor
COPY Cargo.toml Cargo.lock ./
RUN cargo build --release

COPY . .
RUN touch src/main.rs
RUN cargo build --release

# No more second stage because we'd love some cache
# # second stage.
# FROM debian:buster-slim
# RUN apt-get update && apt-get install -y postgresql curl && rm -rf /var/lib/apt/lists/* && mkdir /app
# WORKDIR /app
# COPY --from=builder /app/*.toml ./
# COPY --from=builder /app/target/release/agevote .

EXPOSE 8000

ENTRYPOINT ["./target/release/tcb_backend"]
